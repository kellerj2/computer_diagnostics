# Computer Diagnostics

This package contains nodes for monitoring the CPU, hard drive, and chrony time sync. It publishes DiagnosticArray messages on the /diagnostics topic which is subscribed to by the aggregator_node in the diagnostic_aggregator package.

The cpu_monitor.py node takes two ROS parameters, cpu_warn and cpu_error. They are thresholds that determine what CPU usage levels will trigger WARN and ERROR levels in the DiagnosticArray.

The hd_monitor.py node takes three ROS parameters, filesystem, use_warn, and use_error. use_warn and use_error are analagous to the cpu_monitor.py parameters. The filesystem parameter is the name of the filesystem to monitor. This can be found by running the command 'df', looking the the 'Filesystem' column, and picking which one to use. Internally, this node works by parsing the output from 'df'.

The chrony_monitor.py node takes the seconds_error ROS paramter. If the time offset between the time source and this computer as reported by chrony is greater than the number of seconds specified by seconds_error, an ERROR level is set in the DiagnosticArray.