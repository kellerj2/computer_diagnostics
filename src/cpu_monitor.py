#!/usr/bin/python
import rospy
import subprocess
import os
from diagnostic_msgs.msg import DiagnosticStatus, DiagnosticArray, KeyValue

if __name__ == '__main__':
    rospy.init_node('cpu_monitor', anonymous=True)

    warn_cpu = rospy.get_param("~cpu_warn", 90)
    error_cpu = rospy.get_param("~cpu_error", 95)
    
    diag_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=10)
    array = DiagnosticArray()
    status = DiagnosticStatus()
    status.name = "CPU Usage"
    status.hardware_id = "NUC CPU"
    
    cpu_usage_kv = KeyValue()
    cpu_usage_kv.key = "CPU Usage Percentage"
    cpu_usage_kv.value = ""
    
    while not rospy.is_shutdown():
        cpu_usage = 100 -int(subprocess.check_output("vmstat 1 2 | tail -1 | awk '{print $15}'", shell=True))
        cpu_usage_kv.value = str(cpu_usage)
        
        status.level = DiagnosticStatus.OK
        status.message = "OK"
        if cpu_usage >= warn_cpu:
            status.level = DiagnosticStatus.WARN
            status.message = "Warning: CPU usage is >= " + str(warn_cpu)
        if cpu_usage >= error_cpu:
            status.level = DiagnosticStatus.ERROR
            status.message = "Error: CPU usage is >= " + str(error_cpu)
        
        status.values = [cpu_usage_kv]
        
        array.header.stamp = rospy.Time.now()
        array.status = [status]
        
        diag_pub.publish(array)
        
